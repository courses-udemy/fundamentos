upper = (text) => text.toUpperCase()

module.exports = { upper }
