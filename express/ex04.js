const express = require('express')

const server = express()

server.route('/clients')
  .get((req, res) => res.send('Lista de clientes'))
  .post((req, res) => res.send('Novo clientes'))
  .put((req, res) => res.send('Alerar clientes'))
  .delete((req, res) => res.send('Remover clientes'))

server.use('/api', function(req, res) {
  console.log('Resposta...')
  res.send('<h1>API</h1>')
})

server.listen(3000, () => console.log('Running...'))
