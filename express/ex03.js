const express = require('express')

const server = express()

server.use('/api', (req, res, next) => {
  console.log('inicio...')
  next()
  console.log('fim...')
  console.log('------------')
})

server.use('/api', function(req, res) {
  console.log('Resposta...')
  res.send('<h1>API</h1>')
})

server.listen(3000, () => console.log('Running...'))
