const http = require('http')
const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/html' })
  res.end('<h1>Server working</h1>')
})

const port = 3456
server.listen(port, () => console.log(`Open http://localhost:${port}`))
