const express = require('express')

const server = express()

server.use('/', (req, res, next) => {
  console.log('inicio...')
  next()
  console.log('fim...')
})

server.use('/', function(req, res) {
  console.log('Resposta...')
  res.send('<h1>Olá express</h1>')
})

server.listen(3000, () => console.log('Running...'))
